document.addEventListener('DOMContentLoaded', function() {
  function getRandomInRange(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  let numberH = getRandomInRange(0, 5);
  if ($('.b-error__title').length) {
    $('.b-error__title span')[numberH].style.display='block';
  }

  let $buttonScroll = $('.js-scroll-id');


  $buttonScroll.on('click', function() {
    $('html,body').animate({scrollTop: $(window).innerHeight() + 5}, 400);
  });
});
