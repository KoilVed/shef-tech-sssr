import Swiper from 'swiper';
import slick from 'slick-carousel';

document.addEventListener('DOMContentLoaded', function() {
  let aboutLastSlider = $('.js-slider-home');
  let paramAboutLastSlider = {
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: true,
    infinite: true,
    dots: false,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 769,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 540,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };
  $(aboutLastSlider).slick(paramAboutLastSlider);
});
